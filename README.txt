Skills assessment for Give Directly.

Requires nodejs and npm:
Node: https://nodejs.org/en/download/
NPM: https://www.npmjs.com/get-npm

Installation (*nix machines):
Run the following commands from the top level directory of this project:
$ npm install
$ node index.js

The server will run on port 5000 and exposes the following endpoints:

GET /request
Returns all existing requests for books.

GET /request/id
Returns the existing request for the target book. 
REQUIRED: id (int) - ID of target book

POST /request
Creates a new request for a book.
REQUIRED: email (string) - email of user requesting the book
REQUIRED: title (string) - title of book being requested

DELETE /request/id
Removes an existing request for a book.
REQUIRED: id (int)
