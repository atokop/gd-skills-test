const process = require('process');
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./sqlite-db.js');
const dataLoader = require('./data-loader.js')(db);

const app = express();
app.use(bodyParser.json());

const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => console.log('app running on port: ' + PORT));

const initialize = async () => {
  const insertBook = async (title) => {
    const id = generateId();
    const query = `INSERT INTO books (id, title) VALUES (${id}, '${title}')`;
    await db.query(query)
  }
  await db.init()
  await insertBook("The Colour Of Magic");
  await insertBook("The Colour Of Magic"); // Multiple copies.
  await insertBook("Les Miserables");
  await insertBook("Where Angels Fear To Tread");
}

try {
  initialize();
} catch (err) {
  console.log("Failure initializing DB");
  process.exit(1);
}

const bookRowToRequestOutput = row => {
  return {
    id: row.Id,
    available: row.RequestingUser === null,
    title: row.Title,
    timestamp: row.RequestTimestamp,
  }
}

// Returns all existing requests for books.
app.get('/request', async (req, res) => {
  const allBooks = await dataLoader.getAllBooks();
  const allRequests = allBooks
    .filter(book => book.RequestingUser !== null)
    .map(bookRowToRequestOutput);
  res.status(200).send(allRequests);
});

// Returns the existing request for the target book. 
// REQUIRED: id (int) - ID of target book
// NOTE: Didn't have time to add, but in production would sanitize user input.
app.get('/request/:id', async (req, res) => {
  const id = req.params.id;
  const targetBook = await dataLoader.getBookById(id);
  console.log(targetBook);
  if (targetBook === undefined) {
    res.status(404).send(`No book found with given ID: ${id}.`);
    return;
  }
  if (targetBook.RequestingUser === null) {
    res.status(404).send(`No requests found for book with ID: ${id}.`);
    return;
  }
  res.status(200).send(bookRowToRequestOutput(targetBook));
});

// Creates a new request for a book.
// REQUIRED: email (string) - email of user requesting the book
// REQUIRED: title (string) - title of book being requested
// NOTE: Didn't have time to add, but in production would sanitize user input.
app.post('/request', async (req, res) => {
  const isEmailValid = email => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

  const email = req.body.email;
  const title = req.body.title;

  if (email === undefined) {
    res.status(400).send("Email field must be populated for request.");
    return;
  }
  if (title === undefined) {
    res.status(400).send("Title field must be populated for request.");
    return;
  }
  if (!isEmailValid(email)) {
    res.status(400).send(`Invalid email address: ${email}.`);
    return;
  }
  
  const targetBooks = await dataLoader.getBooksByTitle(title);
  if (targetBooks.length === 0) {
    res.status(404).send(`No book found with given title: ${title}.`);
    return;
  }
  const availableBook = targetBooks.find(book => book.RequestingUser === null);
  const isAvailable = availableBook !== undefined;
  if (!isAvailable) {
    res.status(200).send(bookRowToRequestOutput(targetBooks[0]));
    return;
  }

  const id = availableBook.Id;
  const requestTimestamp = new Date().toISOString();
  await dataLoader.addRequest(availableBook.Id, email, requestTimestamp)
  const response = {
    id: availableBook.Id,
    available: true,
    title: title,
    timestamp: requestTimestamp,
  }
  res.status(200).send(response);
});

// Removes an existing request for a book.
// REQUIRED: id (int)
// NOTE: Didn't have time to add, but in production would sanitize user input.
app.delete('/request/:id', async (req, res) => {
  const id = req.params.id;
  const targetBook = await dataLoader.getBookById(id);
  if (targetBook === undefined) {
    res.status(404).send(`No book found with given ID: ${id}.`);
    return;
  }
  await dataLoader.removeRequest(id);
  res.status(200).send("");
});

const generateId = () => Math.floor(Math.random() * 1000000);
