module.exports = (db) => {
  return {
    getAllBooks: async () => {
      const query = `SELECT * FROM books`;
      return await db.query(query);
    },
    getBookById: async id => {
      const query = `SELECT * FROM books WHERE id=${id}`;
      const result =  await db.query(query);
      if (result.length > 1) {
        throw new Error("Multiple books found with the same id");
      }
      if (result.length === 0) {
        return undefined;
      }
      return result[0];
    },
    getBooksByTitle: async title => {
      const query = `SELECT * FROM books WHERE title='${title}'`;
      return await db.query(query);
    },
    addRequest: async (id, user, timestamp) => {
      const query = `UPDATE books `
        + `SET RequestingUser='${user}', RequestTimestamp='${timestamp}' `
        + `WHERE id=${id}`;
      await db.query(query);
    },
    removeRequest: async id => {
      const query = `UPDATE books `
        + `SET RequestingUser=NULL, RequestTimestamp=NULL `
        + `WHERE id=${id}`;
      await db.query(query);
    }
  }
}
