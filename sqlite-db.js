const sqlite = require('sqlite-async');
const fs = require('fs');

var db;

module.exports = {
  init: async () => {
    console.log("Initializing DB");

    db = await sqlite.open(":memory:");

    const sqlFile = fs.readFileSync('./setup.sql').toString();
    const sqlStatements = sqlFile.split(';').slice(0, -1);

    for (const statement of sqlStatements) {
      await db.run(statement);
    }
  },
  query: async (statement) => {
    console.log("Executing SQL Query: ", statement);
    return await db.all(statement);
  },
  close: async () => {
    console.log("Closing DB");
  }
}
