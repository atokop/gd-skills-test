CREATE TABLE books (
  Id int PRIMARY KEY NOT NULL,
  Title varchar NOT NULL,
  RequestingUser varchar,
  RequestTimestamp varchar
);
